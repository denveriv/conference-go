from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Use the Pexels API
    url = f"https://api.pexels.com/v1/search?query={city},{state}/"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    content = json.loads(r.text)
    print(content["photos"][0]["url"])
    return content["photos"][0]["url"]

def get_weather_data(city, state):
    # Use the Open Weather API
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},01&appid={OPEN_WEATHER_API_KEY}"
    r = requests.get(url)
    content = json.loads(r.text)
    lat = content[0]['lat']
    lon = content[0]['lon']
    cw = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    wr = requests.get(cw)
    content = json.loads(wr.text)
    content = f"Skies: {content['weather'][0]['description']}, Temp: {content['main']['temp']}"
    return content
